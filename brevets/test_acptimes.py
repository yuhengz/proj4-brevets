from acp_times import open_time
from acp_times import close_time
import arrow
import nose

def test_open_time():
    """
    Examples given on rusa.org
    """
    assert open_time(60, 200, '2017-01-01T00:00:00') == arrow.get('2017-01-01T00:00:00').isoformat()
    assert open_time(175, 200, '2017-01-01T00:00:00') == arrow.get('2017-01-01T00:00:00').isoformat()
    assert open_time(205, 200, '2017-01-01T00:00:00') == arrow.get('2017-01-01T00:00:00').isoformat()
    assert open_time(600, 600, '2017-01-01T00:00:00') == arrow.get('2017-01-01T00:00:00').isoformat()
    assert open_time(450, 600, '2017-01-01T00:00:00') == arrow.get('2017-01-01T00:00:00').isoformat()
    assert open_time(900, 1000, '2017-01-01T00:00:00') == arrow.get('2017-01-01T00:00:00').isoformat()
def test_close_time():
    '''
    Test close time
    '''
    assert close_time(60, 200, '2017-01-01T00:00:00') == arrow.get('2017-01-01T00:00:00').isoformat()
    assert close_time(175, 200, '2017-01-01T00:00:00') == arrow.get('2017-01-01T00:00:00').isoformat()
    assert close_time(450, 200, '2017-01-01T00:00:00') == arrow.get('2017-01-01T00:00:00').isoformat()
    assert close_time(205, 200, '2017-01-01T00:00:00') == arrow.get('2017-01-01T00:00:00').isoformat()
    assert close_time(600, 600, '2017-01-01T00:00:00') == arrow.get('2017-01-01T00:00:00').isoformat()
    assert close_time(900, 1000, '2017-01-01T00:00:00') == arrow.get('2017-01-01T00:00:00').isoformat()
